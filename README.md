# Modular WordPress with ACF Flexible Content

Wiele osób wybiera WordPress z powodu elastyczności i kontroli jaką zapewnia ten CMS, jednak często okazuje się, że jedyną rzeczą jaką użytkownik może edytować na stronie jest treść posta. Na przestrzeni lat aby wyjść na przeciw wymaganiom naszych klientów, Chop Chop wypracował wiele rozwiązań, a jednym z nich jest modularność motywów WordPress. Tworzenie w oparciu o moduły wymaga odpowiedniego mindsetu, należy przede wszystkim przestać myśleć o stronie internetowej jako kolekcji podstron i templatek, ale jako o zbiorze modułów oraz systemie który nimi zarządza. 

## Czym właściwie są moduły i co zyskujemy korzystając z nich?
O modułach można myśleć jak o Lego - klocki są wymienne i mogą być łączone ze sobą na niezliczoną ilość sposobów, tym samym tworząc różnorodną i spójną całość. Przenosząc tę koncepcję do WordPress otrzymujemy produkt, który pozwala na tworzenie zróżnicowanych layoutów bez konieczności edycji chociażby jednej linii kodu. Modularyzacja obniża wymagany poziom umiejętności potrzebny do tworzenia dynamicznych treści, oszczędza czas i daje nam dowolność w budowaniu oraz modyfikowaniu podstron.

## Jak wygląda to w praktyce?
Załóżmy, że mamy już gotową stronę, ale chcielibyśmy dodać do niej więcej informacji o naszym produkcie. Dzięki odpowiednio przygotowanym modułom nie ma nic prostszego - wszystko w zasadzie sprowadza się do dodania nowego bloku, wypełnienia go treścią i gotowe.

![alt](/page-preview.gif)

## Jak stworzyć modularny motyw WordPress?
Sposobów jest wiele, jednak w tym artykule skupimy się na użyciu pola Flexible Content, który jest częścią pluginu Advanced Custom Fields (ACF). Jeśli pierwszy raz słyszysz o tym pluginie gorąco polecamy najpierw zapoznać się z oficjalną dokumentacją dostępną [tutaj](https://www.advancedcustomfields.com/resources/)

#### Proces modularyzacji składa się z 3 etapów
1. Zdefiniowana modułów w panelu WordPress za pomocą ACF Flexible Content
2. Zakodowanie modułów w oparciu o stworzone przez nas pola
3. Budowanie podstron z gotowych modułów

### 1. Zdefiniowanie modułów w panelu WordPress za pomocą ACF Flexible Content

Po zainstalowaniu oraz aktywowaniu ACF PRO w panelu admina pojawia się "Custom Fields".

![alt](/1.png)

Klikamy w ten element a następnie w ***"Add New"***

![alt](/1a.png)

Nazwijmy tę grupę "Flexible Page Content Blocks", następnie dodajemy nowe pole, a jego typ ustawiamy na Flexible.

![alt](/2.png)

Następnie możemy przystąpić do tworzenia naszych modułów. Aby to zrobić należy dodać nowy Layout i zdefiniować pola które będą częścią danego modułu. Oto jak wyglądają przykładowe moduły, które później będą wykorzystywane na naszej stronie

![alt](/3.png)
![alt](/4.png)

Ten etap jest kluczowy i bardzo czasochłonny. Wymaga dokładnej analizy designu i podzielenia go na niezależne bloki które staną się naszymi modułami. Po zdefiniowaniu bloków ostatnią rzeczą jaką należy zrobić jest ustawienie miejsca w WordPress gdzie chcemy korzystać ze stworzonej grupy pól.

![alt](/5.png)

Teraz wystarczy zapisać zmiany używając przycisku ***Update***

### 2. Zakodowanie modułów w oparciu o stworzone przez nas pola

Aby zdefiniowane przez nas moduły wyświetlały się w motywie należy je odpowiednio zakodować. W poprzednim etapie wybraliśmy, że pola mają się wyświetlać na Page Template, dlatego też w katalogu naszego motywu tworzymy plik o nazwie ***page.php***.

```
<?php
/**
 * The flexible page template.
 *
 * @package WordPress
 * @subpackage preview-template
 * @since 1.0.0
 */

get_header();
?>
	<main class="main-content">
		<?php
		// Place where we want to display out modules so we will put flexible content logic here
		?>
	</main>
<?php
get_footer();
```

Następnie musimy dodać logikę, która będzie odpowiadała za wyśweietlanie modułów, z pomocą przychodzi nam [dokumentacja](https://www.advancedcustomfields.com/resources/flexible-content/) ACF Pro Flexible Content.
Kluczową rzeczą aby zrozumieć dzialanie Flexible Content jest pojęcie działania ***pętli*** - podczas każej iteracji następuje sprawdzenie jaki typ layoutu został użyty i wyświetlana jest cała logika którą zdefiniowaliśmy w tym layoucie. Tak wygląda implementacja przytoczonego wcześniej przykładu modułu Hero.

```
<?php

// check if the flexible content field has rows of data
if( have_rows('page_content') ):

	// loop through the rows of data
	while ( have_rows('page_content') ) : the_row();

		if( get_row_layout() == 'block_hero' ):
			$background_image = get_sub_field('background_image');
			$content = get_sub_field('content');
			$cta_button = get_sub_field('cta_button');

			// Display content from that module
			echo $content;
		endif;

		// Then we can check if get_row_layout() === other module and display its data

	endwhile;

else :
	// no layouts found
endif;

?>
```

Oczywiście jest to bardzo uproszczony przykład do którego można wprowadzic wiele usprawnień jak chociażby podział layoutów na osobne niezależne pliki i dynamiczne includowanie ich w pętli. W każdym razie jest to absolutne minimum, aby prawidłowo wyświetlić moduł Hero w naszym theme.

### 3. Budowanie podstron z gotowych modułów
Jeżeli wykonaliśmy poprzednie etapy możemy zacząć tworzyć nasze podstrony. W panelu admina przechodzimy do ***Pages***, następnie klikamy ***Add New*** i pierwsze co powinno rzucić nam się w oczy jest stworzone przez nas pole Page Content. Tutaj możemy dodawać nasze moduły, zmieniać ich treść oraz dowolnie ustawiać ich pozycję na stronie

![alt](/6.png)

![alt](/block-rearange.gif)

Na tym etapie tworzenie kolejnych stron jest banalnie proste. Gwarantujemy, że klienci będą bardzo zadowoleni z takiego rozwiązania. Należy pamiętać, że omówiona metoda jest jedną z wielu na osiągnięcie modularności w WordPress. Na chwilę obecną wykorzystanie ACF jest bez wątpienia najpopularniejszym rozwązaniem. Ciekawą alternatywą dla ACF Flexible Content jest Wordpress Gutenberg.